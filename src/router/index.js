import Vue from 'vue'
import Router from 'vue-router'
// import Load from '@/components/load'
import Home from '@/components/home'
import Upload from '@/components/upload'
import Share from '@/components/share'

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'Load',
    //   component: Load
    // },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/upload',
      name: 'Upload',
      component: Upload
    },
    {
      path: '/share',
      name: 'Share',
      component: Share
    }
  ]
})
